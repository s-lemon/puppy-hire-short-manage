const path = require('path');
const webpack = require('webpack')

function resolve(dir) {
    console.log(__dirname);
    return path.resolve(__dirname, dir)
}
// 项目的主要配置文件
module.exports = {
    // webpack 配置进行更细粒度的修改  https://cli.vuejs.org/zh/config/#chainwebpack
    chainWebpack: (config) => {
        //修改文件引入自定义路径
        config.resolve.alias
            .set('@', resolve('src'))
            .set('@assets', resolve('src/assets'))
            .set('@style', resolve('src/assets/style'));
    },
    configureWebpack: {
        plugins: [
            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery",
                "windows.jQuery": "jquery"
            })
        ]
    },
    lintOnSave: false,
    devServer: {
        proxy: {
            // proxy all requests starting with /api to jsonplaceholder
            '/api': {
                target: 'http://10.61.180.133:8088',   //代理接口
                changeOrigin: true,
                // ws: true,
                pathRewrite: {
                    '^/api': '/puppy'    //代理的路径
                }
            }
        }
    }
}