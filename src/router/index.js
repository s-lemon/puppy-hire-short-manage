import Vue from 'vue'
import VueRouter from 'vue-router'
import HouseDetail from '../views/HouseDetail.vue'
import AuditHouse from '../views/AuditHouse.vue'
import Data from '../views/Data.vue'
import Home from '../views/Home.vue'
import CommentList from '../views/CommentList.vue'
import ThemeList from '../views/ThemeList.vue'
import LandlordDiary from '../views/LandlordDiary.vue'
import UserList from '../views/UserList.vue'

Vue.use(VueRouter)

const routes = [
    {
      path: '/',
      name: 'home',
      component: Home
   },
    {
      path: '/housedetail/:id',
      name: 'housedetail',
      component: HouseDetail
    },
    {
      path:'/audithouse',
      name:'audithouse',
      component:AuditHouse
    },
    {
      path:'/data',
      name:'data',
      component:Data,
      children: [
        {
          path: '',
          component: ThemeList
        },
        {
          path:'commentlist',
          name:'commentlist',
          component:CommentList
        },
        {
          path:'landlorddiary',
          name:'landlorddiary',
          component:LandlordDiary
        }
      ]
    },
    {
      path:'/userlist',
      name:'userlist',
      component:UserList
    }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
