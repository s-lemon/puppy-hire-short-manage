import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    url: 'http://localhost:8088/puppy/admin/',
    imageUrl:'http://localhost:8088/puppy/',
    loginState: 'LogOut',
    adminInfo: {}
  },
  getters: {
    getLoginState: state => {
      if(state.loginState == 'LogOut'){
        let login = window.sessionStorage.getItem('loginState');
        if(login == 'LogIn'){
          state.loginState = login;
        }
      }
      return state.loginState;
    },
    getAdminInfo: state => {
      if(state.adminInfo && state.adminInfo.userId){
        let info = JSON.parse(window.sessionStorage.getItem('adminInfo'));
        if(info){
          state.adminInfo = info;
        }
      }
      return state.adminInfo;
    }
  },
  mutations: {
    turnLoginState(state, {adminInfo, loginState}){
      console.log(loginState);
      state.loginState = loginState;
      state.adminInfo = adminInfo;
      window.sessionStorage.setItem('adminInfo', JSON.stringify(adminInfo));
      window.sessionStorage.setItem('loginState', loginState);
    }
  },
  actions: {
  },
  modules: {
  }
})
